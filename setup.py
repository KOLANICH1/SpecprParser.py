#!/usr/bin/env python3
from pathlib import Path
from setuptools import setup

from kaitaiStructCompile.postprocessors import permissiveDecoding

thisDir = Path(__file__).parent

formatsPath = str(thisDir / "kaitai_struct_formats")
kaitaiCfg = {
	"formats": {
		"specpr.py": {
			"path": "scientific/spectroscopy/specpr.ksy",
			"postprocess": ["permissiveDecoding"]
		}
	},
	"formatsRepo": {
		"localPath" : formatsPath,
		"update": True
	},
	"outputDir": "SpecprParser",
	"inputDir": formatsPath
}

setup(use_scm_version=True, kaitai=kaitaiCfg)
